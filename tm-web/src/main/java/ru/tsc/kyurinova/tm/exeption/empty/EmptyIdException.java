package ru.tsc.kyurinova.tm.exeption.empty;

import org.jetbrains.annotations.NotNull;
import ru.tsc.kyurinova.tm.exeption.AbstractException;

public class EmptyIdException extends AbstractException {

    public EmptyIdException() {
        super("Error. Id is empty.");
    }

    public EmptyIdException(@NotNull String modelName) {
        super("Error. " + modelName + " id is empty.");
    }

}
