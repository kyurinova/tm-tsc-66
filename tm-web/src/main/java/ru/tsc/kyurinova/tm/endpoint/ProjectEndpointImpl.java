package ru.tsc.kyurinova.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.tsc.kyurinova.tm.api.service.dto.IProjectDTOService;
import ru.tsc.kyurinova.tm.client.ProjectRestEndpointClient;
import ru.tsc.kyurinova.tm.dto.model.ProjectDTO;

import java.util.List;

@RestController
@RequestMapping("/api/projects")
public class ProjectEndpointImpl implements ProjectRestEndpointClient {

    @Autowired
    private IProjectDTOService projectService;

    @Override
    @GetMapping("/findAll")
    public List<ProjectDTO> findAll() throws Exception {
        return projectService.findAll();
    }

    @NotNull
    @Override
    @PostMapping("/add")
    public ProjectDTO add(@RequestBody final @NotNull ProjectDTO project) throws Exception {
        return projectService.add(project);
    }

    @Override
    @PostMapping("/save")
    public void save(@RequestBody final @NotNull ProjectDTO project) throws Exception {
        projectService.update(project);
    }

    @Nullable
    @Override
    @GetMapping("/findById/{id}")
    public ProjectDTO findById(@PathVariable("id") final @NotNull String id) throws Exception {
        return projectService.findOneById(id);
    }

    @Override
    @GetMapping("/existsById/{id}")
    public boolean existsById(@PathVariable("id") final @NotNull String id) throws Exception {
        return (projectService.findOneById(id) != null);
    }

    @Override
    @GetMapping("/count")
    public long count() throws Exception {
        return projectService.count();
    }

    @Override
    @PostMapping("/deleteById/{id}")
    public void deleteById(@PathVariable("id") final @NotNull String id) throws Exception {
        projectService.removeById(id);
    }

    @Override
    @PostMapping("/delete")
    public void delete(@RequestBody final @NotNull ProjectDTO project) throws Exception {
        projectService.remove(project);
    }

    @Override
    public void clear() throws Exception {
        projectService.clear();
    }

}
