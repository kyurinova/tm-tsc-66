package ru.tsc.kyurinova.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.kyurinova.tm.event.ConsoleEvent;
import ru.tsc.kyurinova.tm.enumerated.Role;
import ru.tsc.kyurinova.tm.util.TerminalUtil;

@Component
public class ProjectFinishByNameListener extends AbstractProjectListener {

    @NotNull
    @Override
    public String command() {
        return "project-finish-by-name";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Finish project by name...";
    }

    @Override
    @EventListener(condition = "@projectFinishByIndexListener.command() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("Enter name");
        @NotNull final String name = TerminalUtil.nextLine();
        projectEndpoint.finishByNameProject(sessionService.getSession(), name);
    }

    @Nullable
    @Override
    public Role[] roles() {
        return Role.values();
    }

}
