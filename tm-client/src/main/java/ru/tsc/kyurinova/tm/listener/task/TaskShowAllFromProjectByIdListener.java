package ru.tsc.kyurinova.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.kyurinova.tm.event.ConsoleEvent;
import ru.tsc.kyurinova.tm.endpoint.SessionDTO;
import ru.tsc.kyurinova.tm.enumerated.Role;
import ru.tsc.kyurinova.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.kyurinova.tm.endpoint.ProjectDTO;
import ru.tsc.kyurinova.tm.util.TerminalUtil;

import java.util.Optional;

@Component
public class TaskShowAllFromProjectByIdListener extends AbstractTaskListener {

    @NotNull
    @Override
    public String command() {
        return "task-show-by-project-id";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Show all tasks by project id...";
    }

    @Override
    @EventListener(condition = "@taskShowAllFromProjectByIdListener.command() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        @Nullable final SessionDTO session = sessionService.getSession();
        System.out.println("Enter project id");
        @NotNull final String projectId = TerminalUtil.nextLine();
        @Nullable final ProjectDTO project = projectEndpoint.findByIdProject(session, projectId);
        Optional.ofNullable(project).orElseThrow(ProjectNotFoundException::new);
        System.out.println(projectTaskEndpoint.findAllTaskByProjectId(session, projectId));
    }

    @Nullable
    @Override
    public Role[] roles() {
        return Role.values();
    }

}
